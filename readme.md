# PUSAQ RUNA

Página web para www.pusaqruna.com, incluye Tinkuy.TEC que utiliza Servicios Web de Moodle con una configuración e interfaz personalizada.




## Requerimientos de Instalación


- Laravel 5.2.8
- PHP >= 5.3
- MySQL 5.6
- Composer




## Configuración del Moodle


- Instalar Moodle (v3.0.2+) y acceder como administrador
- Habilitar y configurar servicios web de Moodle. [Guía aquí](https://docs.moodle.org/30/en/Using_web_services)
- Agregar las siguientes funciones al servicio
core_user_create_users
core_user_update_users




## Instalación Local


- Clonar este proyecto, ejecutar desde una terminal:
```
git clone https://gitlab.com/yg-apaza/pusaqruna.git
```
- Desde la raiz del proyecto, ejecutar
```
composer install
cp .env.example .env
php artisan key:generate
```
- Ejecutar el script.sql en una base de datos (diferente a la creada para el Moodle)
- Editar el archivo .env, modificar APP_DEBUG a false
- Editar el archivo .env, modificar DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD dependiendo de la configuración de la base de datos propia de la web.
- Editar el archivo .env, modificar MOODLE_URL (URL del directorio principal de Moodle sin incluir el caracter '/' al final), MOODLE_TOKEN (Token de usuario de servicio web de Moodle)
- Ejecutar y lanzar el servidor:
```
php artisan serve
```
- Abrir la direccion especificada en un navegador (comunmente localhost:8000)




## Lanzamiento en un servicio de hosting con Apache Web Server


- Instalar Moodle (v3.0.2+) y acceder como administrador

- Habilitar y configurar servicios web de Moodle. [Guía aquí](https://docs.moodle.org/30/en/Using_web_services)

- Agregar trabajos de Cron en el CPanel cada 30 min
```
php -q /home/pusaq/public_html/moodlews/admin/cli/cron.php
```
- Instalar los plugins

- Email based self registration [aqui](https://docs.moodle.org/25/en/Email-based_self-registration), deshabilitar
- Deshabilitar Boton de ingreso para invitados, Administracion del sitio > Plugins > Autenticacion > Gestionar autenticacion
- Agregar las siguientes funciones al servicio
local_wstinkuytec_create_user

- Habilitar login por correo (Verificar)

- Agregar campos Kallpa (kallpa), Profesión (profesion), Especialidad (especialidad)

- Modificar politicas de la contraseña, Tamaño minimo: 6, 0 caracteres especiales, numeros, mayusculas y minusculas minimas

- Modificar politicas de usuario, Administracion del sitio > Usuarios > Permisos > Politicas para el Usuario, Mostrar identidad de usuario: Numero de ID (DNI), Direccion Email

- Desde la raiz del proyecto, ejecutar
```
composer install
cp .env.example .env
php artisan key:generate
```

- Ejecutar el script.sql en una base de datos (diferente a la creada para el Moodle)
- Editar el archivo .env, modificar DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD dependiendo de la configuración de la base de datos propia de la web.
- Editar el archivo .env, modificar MOODLE_URL (URL del directorio principal de Moodle sin incluir el caracter '/' al final, probablemente 'localhost'), MOODLE_TOKEN (Token de usuario de servicio web de Moodle)
- Eliminar la carpeta utils del proyecto
- Eliminar la carpeta .git
- Eliminar los archivos .gitattributes, .gitignore
- Eliminar este readme.md
- Subir el contenido de la carpeta 'public' a 'public_html'
- Subir el resto del contenido a una carpeta un nivel arriba de 'public_html' denominada 'pusaqruna'
- La estructura debe quedar:
> /home/pusaq/pusaqruna
Carpetas: app, bootstrap, config, database, resources, storage, tests, vendor
Archivos: .env, artisan, composer.json, composer.lock, gulpfile.js, package.json, phpunit.xml, server.php
> /home/pusaq/public_html
Carpetas: css, font-awesome, img, inforobot, js
Archivos: .htaccess, favicon.ico, index.php, robots.txt, web.config