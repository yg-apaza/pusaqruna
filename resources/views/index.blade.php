@extends('layout.pusaqruna', ['usuario' => session('usuario')])

@section('contenido')
<div class="bg_color">
	<div class="container">
		<div class="col-md-6 service_2-left">
	    	<h2>¿QUÉ ES UNA PLATAFORMA E-LEARNING?</h2>
	 	</div>
		<div class="col-md-6 service_2-right">
			<p>Es un entorno virtual de enseñanza y aprendizaje. Es decir, una aplicación web que integra un conjunto de herramientas para la enseñanza en línea, permitiendo el aprendizaje no presencial o virtual (e-learning).</p>
			<br>
			<p>El objetivo principal de una plataforma e-Learning es crear y gestionar espacios de enseñanza y aprendizaje en Internet, donde los profesores y los estudiantes puedan interactuar durante su proceso de formación.</p>
		</div>
	 	<div class="clearfix"> </div>
	</div>
</div>

<!--div class="grid_1">

	<div class="container">
		<div class="col-md-12">
			<div class="news">
				<h1>ARTÍCULOS RECIENTES</h1>
				<a href="{{ URL::asset('blog') }}" class="read-more">Ver todos los artículos</a>
				<br><br>
				<div class="section-content">
					<article>
					    <figure class="date"><i class="fa fa-file-o"></i>16-01-2016</figure>
					    <a href="#">¿Quieres formarte por tu cuenta?</a>
					</article>
					<article>
					    <figure class="date"><i class="fa fa-file-o"></i>16-01-2016</figure>
					    <a href="#">España continúa siendo líder en la generación de cursos MOOC.</a>
					</article>
					<article>
					    <figure class="date"><i class="fa fa-file-o"></i>11-01-2016</figure>
					    <a href="#">Juan Medina: "No es difícil preparar un MOOC".</a>
					</article>
					<article>
					    <figure class="date"><i class="fa fa-file-o"></i>29-12-2015</figure>
					    <a href="#">Encontrando tesoros en la red.</a>
					</article>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div-->
@stop