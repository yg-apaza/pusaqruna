@extends('layout.pusaqruna', ['usuario' => session('usuario')])

@section('contenido')

<div class="container">
	<div class="contenido">
		<div class="col-md-6 faculty_grid micurso">
			<figure class="team_member">
				<img src="{{ URL::asset('img/banner/comercio.jpg') }}" class="img-responsive wp-post-image" width="100%" height="100%/">
				<div></div>
		  		<figcaption>
		  			<h2 class="person-title"><strong>DEPARTAMENTO COMERCIAL</strong></h2>
	  				<div class="person-social">
	  					<a class="shortcode_but large" href="#" target="_self" style="color:#ffffff; background-color:#d64f4f; "> ENTRAR </a>
	  		  		</div>
		  	   	</figcaption>
			</figure>
		</div>
		<div class="col-md-6 faculty_grid micurso">
			<figure class="team_member">
				<img src="{{ URL::asset('img/banner/tinkuytec.jpg') }}" class="img-responsive wp-post-image" width="100%" height="100%/">
				<div></div>
		  		<figcaption>
		  			<h2 class="person-title"><b>PROGRAMA TINKUY.TEC</b></h2>
	  				<div class="person-social">
	  					<a class="shortcode_but large" href="{{ URL::asset('tinkuytec') }}" target="_self" style="color:#ffffff; background-color:#d64f4f; "> IR A TINKUYTEC </a>
	  		  		</div>
		  	   	</figcaption>
			</figure>
		</div>
		<div class="col-md-6 faculty_grid micurso">
			<figure class="team_member">
				<img src="{{ URL::asset('img/banner/consultoria.jpg') }}" class="img-responsive wp-post-image" width="100%" height="100%/">
				<div></div>
		  		<figcaption>
		  			<h2 class="person-title"><b>CONSULTORIA EDUCATIVA</b></h2>
	  				<div class="person-social">
	  					<a class="shortcode_but large" href="#" target="_self" style="color:#ffffff; background-color:#d64f4f; "> ENTRAR </a>
	  		  		</div>
		  	   	</figcaption>
			</figure>
		</div>
		<div class="col-md-6 faculty_grid micurso">
			<figure class="team_member">
				<img src="{{ URL::asset('img/banner/inforobot.jpg') }}" class="img-responsive wp-post-image" width="100%" height="100%/">
				<div></div>
		  		<figcaption>
		  			<h2 class="person-title"><b>INFOROBOT</b></h2>
	  				<div class="person-social">
	  					<a class="shortcode_but large" href="{{ URL::asset('inforobot') }}" target="_self" style="color:#ffffff; background-color:#d64f4f; "> IR A INFOROBOT </a>
	  		  		</div>
		  	   	</figcaption>
			</figure>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<!--div class="grid_1">

	<div class="container">
		<div class="col-md-12">
			<div class="news">
				<h1>ARTÍCULOS RECIENTES</h1>
				<a href="{{ URL::asset('blog') }}" class="read-more">Ver todos los artículos</a>
				<br><br>
				<div class="section-content">
					<article>
					    <figure class="date"><i class="fa fa-file-o"></i>16-01-2016</figure>
					    <a href="#">¿Quieres formarte por tu cuenta?</a>
					</article>
					<article>
					    <figure class="date"><i class="fa fa-file-o"></i>16-01-2016</figure>
					    <a href="#">España continúa siendo líder en la generación de cursos MOOC.</a>
					</article>
					<article>
					    <figure class="date"><i class="fa fa-file-o"></i>11-01-2016</figure>
					    <a href="#">Juan Medina: "No es difícil preparar un MOOC".</a>
					</article>
					<article>
					    <figure class="date"><i class="fa fa-file-o"></i>29-12-2015</figure>
					    <a href="#">Encontrando tesoros en la red.</a>
					</article>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div-->
@stop