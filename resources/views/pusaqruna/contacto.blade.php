@extends('layout.pusaqruna', ['usuario' => session('usuario')])

@section('contenido')
<div class="courses_banner">
	<div class="container">
		<h3>CONTACTO</h3>
		<div class="breadcrumb1">
	        <ul>
	            <li class="icon6"><a href="{{ URL::asset('/') }}">Inicio</a></li>
	            <li class="current-page">Contacto</li>
	        </ul>
    	</div>
	</div>
</div>

<div class="features">

	<div class="container">
		<!--h1>Ubícanos</h1>
		<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1913.7346008939944!2d-71.53201614204487!3d-16.40098002409974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91424a5681725f33%3A0x1b3fc3c40676eca2!2sOctavio+Mu%C3%B1oz+Najar+140%2C+Arequipa!5e0!3m2!1ses!2spe!4v1452859035508" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div-->
		<div class="wrapper">
			<div class="col_1">
				<i class="fa fa-home  icon2"></i>
				<div class="box">
					<p class="marTop9">Calle Octavio Muñóz Nájar 140<br>Oficina 209 (2do piso)</p>
				</div>
			</div>
			<div class="col_1">
				<i class="fa fa-phone  icon2"></i>
				<div class="box">
					<p class="marTop9">993533036<br>959390596</p>
				</div>
			</div>

			<div class="col_2">
				<i class="fa fa-envelope icon2"></i>
				<div class="box">
					<p class="m_6"><a href="mailto:pusaqrunainfo@gmail.com" class="link4">pusaqrunainfo@gmail.com</a></p>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>

	<div class="bg_color">
		<div class="container">
		  	<h2>NUESTRO EQUIPO</h2>
	        <div class="col-md-4">
                <img src="{{ URL::asset('img/team/1.jpg') }}" class="img-responsive img-circle" alt=""/> 
				<h3>Mg. Fabiola Frisancho Ramírez</h3>
				<p>General Manager</p>
	        </div>
	        <div class="col-md-8">
	        	dsssdd
	        </div>
	        <div class="clearfix"> </div>
		</div>
	</div>
	<a name="mensaje"></a>
	<div class="container">
		<form class="contact_form">
			<h2>Déjanos un mensaje</h2>
			<div class="col-md-6 grid_6">
				<input type="text" class="text" value="Nombre" placeholder="Nombre" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Nombre';}">
				<input type="text" class="text" value="Correo" placeholder="Correo" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Correo';}">
			</div>
			<div class="col-md-6 grid_6">
				<textarea value="Mensaje" placeholder="Mensaje" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Mensaje';}">Mensaje</textarea>
			</div>
			<div class="clearfix"> </div>
			<div class="btn_3">
			  	<a href="#" class="more_btn" data-type="submit">Enviar</a>
			</div>
		</form>	
	</div>

</div>
@stop