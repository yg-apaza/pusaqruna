<!DOCTYPE HTML>
<html>
<head>
    <title>PUSAQ RUNA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="TI, Educacion Online, Pusaq Runa, Aprender, Docentes, Robótica" />
    <link href="{{ URL::asset('css/bootstrap-3.1.1.min.css') }}" rel='stylesheet' type='text/css' />
    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/modernizr-custom.js') }}"></script>
    <link href="{{ URL::asset('css/style.css') }}" rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href="{{ URL::asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet"> 
    <script>
        $(document).ready(function(){
            $(".dropdown").hover(
                function() {
                    $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                    $(this).toggleClass('open');
                },
                function() {
                    $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                    $(this).toggleClass('open');       
                }
            );
        });
    </script>
</head>

<body>
    <div class="navbar-fixed-top">
        <nav class="navbar navbar-default" role="navigation">
        	<div class="container">
        	    <div class="navbar-header">
        	        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        		        <span class="sr-only">Toggle navigation</span>
        		        <span class="icon-bar"></span>
        		        <span class="icon-bar"></span>
        		        <span class="icon-bar"></span>
        	        </button>
                    <a href="{{ URL::asset('/') }}"><img src="{{ URL::asset('img/brand.png') }}" class="img-responsive"/></a>
        	    </div>
        	    <div class="clearfix"></div>
            </div>
            <!--/.navbar-collapse-->
        </nav>

        <nav class="navbar nav_bottom" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header nav_2">
                    <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div> 

                <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                    <ul class="nav navbar-nav nav_1">
                        <li><a href="{{ URL::asset('/') }}">INICIO</a></li>
                        <li><a href="{{ URL::asset('/') }}">NUESTRO PERFIL</a></li>
                        <li><a href="{{ URL::asset('/') }}">SERVICIOS</a></li>
                        <li><a href="{{ URL::asset('contacto') }}">CONTACTO</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    
    @yield('contenido')

    <div class="footer">
        <div class="container">
            <div class="col-md-4">
                <img src="{{ URL::asset('img/logo.png') }}" width="220px" height="220px">
                <br>
                <center>
                    <ul class="social-nav icons_2 clearfix">
                        <li><a href="https://twitter.com/roboticaaqp" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/roboticaaqp/" class="facebook" target="_blank"> <i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://plus.google.com/events/c05vvk1oiesbbdcb40hqa33h9us?authkey=CPvAv6z_i8KZTw" class="google-plus" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="https://www.facebook.com/roboticaaqp/" class="linkedin" target="_blank"> <i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </center>
            </div>

            <div class="col-md-4">
                <h3>INFORMES</h3>
                <address>
                        <strong>Contacto:</strong>
                        <br>
                        <span>Srta. Pamela Tejada Paucar</span>
                        <br>
                        <strong>Horario de atención:</strong>
                        <br>
                        <span>Lunes a viernes: 09:00 a.m. a 01:00 p.m. y de 02:00 p.m. a 06:00 p.m.</span>
                        <br>
                        <span>Sábados: 08:30 a.m. a 04:00 p.m.</span>
                        <br>
                </address>
            </div>

            <div class="col-md-4">
                <h3>CONTÁCTANOS</h3>
                <address>
                    <strong>Dirección:</strong>
                    <br>
                    <span>Calle Octavio Muñóz Nájar 140 Of. 209.</span>
                    <br>
                    <span>Centro Comercial "La Diagonal"</span>
                    <br>
                    <span>Arequipa, Perú</span>
                    <br>
                    <strong>Teléfonos:</strong>
                    <br>
                    <span>993533036 - 959390596</span>
                    <br>
                    <strong>Correo:</strong>
                    <br>
                    <a href="mailto:pusaqrunainfo@gmail.com?subject=Contactar">pusaqrunainfo@gmail.com</a>
                    <br>
                </address>
            </div>
            
            <div class="clearfix"> </div>

            <div class="copy">
                <p>Copyright © 2016, Pusaq Runa SAC. Todos los derechos reservados.</p>
                <p>Diseñado por <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
            </div>

        </div>
    </div>
    <script src="{{ URL::asset('js/script.js') }}"></script>
</body>
</html>
