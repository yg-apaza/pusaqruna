<!DOCTYPE HTML>
<html>
<head>
	<title>Tinkuy.TEC - PUSAQ RUNA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="TI, Educacion Online, Pusaq Runa, Aprender, Docentes, Robótica" />
	<link href="{{ URL::asset('css/bootstrap-3.1.1.min.css') }}" rel='stylesheet' type='text/css' />
    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/modernizr-custom.js') }}"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>

    <link href="{{ URL::asset('css/style.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ URL::asset('css/tinkuytec.css') }}" rel='stylesheet' type='text/css' />
    <link href="{{ URL::asset('css/progress.css') }}" rel='stylesheet' type='text/css' />

    <link href="{{ URL::asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet"> 
    <link href="{{ URL::asset('css/animate.css') }}" rel="stylesheet" type="text/css" media="all">

	<script src="{{ URL::asset('js/metisMenu.min.js') }}"></script>
	<script src="{{ URL::asset('js/custom.js') }}"></script>
	<link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet">
	<script src="{{ URL::asset('js/index.js') }}"></script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<script>
        $(document).ready(function(){
            $(".dropdown").hover(
                function() {
                    $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                    $(this).toggleClass('open');
                },
                function() {
                    $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                    $(this).toggleClass('open');       
                }
            );
        });
    </script>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<div class=" sidebar" role="navigation">
            <div class="navbar-collapse">
				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
					<ul class="nav" id="side-menu">
						<li><a href="{{ URL::asset('/tinkuytec') }}" class="active"><i class="fa fa-home nav_icon"></i>Inicio</a></li>
						 @if($usuario)
						<li>
							<a href="#"><i class="fa fa-user nav_icon"></i>Mis datos<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li><a href="{{ URL::asset('tinkuytec/miscursos') }}">Mis cursos</a></li>
								<li><a href="{{ URL::asset('tinkuytec/perfil#medallas') }}">Mis medallas</a></li>
							</ul>
						</li>
						@endif
						<li><a href="{{ URL::asset('tinkuytec/cursos') }}"><i class="fa fa-book nav_icon"></i>Cursos</a></li>
						<li>
							<a href="#"><i class="fa fa-users nav_icon"></i>Conócenos<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li><a href="{{ URL::asset('tinkuytec/conocenos') }}">Conoce Tinkuy.TEC</a></li>
								<li><a href="{{ URL::asset('tinkuytec/normas') }}">Normas de Trabajo</a></li>
								<li><a href="{{ URL::asset('tinkuytec/unete') }}">Unirse a Tinkuy.TEC</a></li>
							</ul>
						</li>
						<li><a href="{{ URL::asset('/') }}" class="active"><i class="fa fa-sign-in nav_icon"></i>Volver a PusaqRuna</a></li>
					</ul>
				</nav>
			</div>
		</div>

		<div class="sticky-header header-section ">
			<div class="header-left">
				<button id="showLeftPush"><i class="fa fa-bars"></i></button>
				<div class="logo">
					<a href="{{ URL::asset('tinkuytec') }}">
						<h1>TINKUY.TEC</h1>
						<img src="{{ URL::asset('img/brand.png') }}" width=80px/>
					</a>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="profile_details">
				@if($usuario)
				<ul class="nav navbar-nav">
		        	<li class="dropdown">
			            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                        <!--i class="fa fa-user"></i-->
	                        <img src="{{ session('urlAvatar') }}" class="img-circle" alt="Avatar" width="40" height="40"> 
	                        <span>{{ $usuario['nombres'].' '.$usuario['apellidos'] }}</span>
	                    </a>
			            <ul class="dropdown-menu" style="display: none;">
				            <li><a href="{{ URL::asset('tinkuytec/perfil') }}"><span><i class="fa fa-star"></i><span> Mi perfil</span></span></a></li>
				            <li><a href="{{ URL::asset('tinkuytec/salir') }}"><span><i class="fa fa-sign-out"></i><span> Salir</span></span></a></li>
				        </ul>
			        </li>
    		    </ul>
				@else
				<div class="loginbtn">
					<a href="{{ URL::asset('tinkuytec/registro') }}" class="btn btn-default btn-lg" role="button">REGISTRARSE</a>
					<a href="{{ URL::asset('tinkuytec/acceso') }}" class="btn btn-default btn-lg" role="button">ENTRAR</a>
				</div>
				@endif
			</div>			
		</div>

		<div id="page-wrapper">

			@yield('contenido')

			<div class="footer">
				<p>Copyright &copy; 2016, <a href="{{ URL::asset('/') }}"> Pusaq Runa SAC</a>. Todos los derechos reservados.</p>
				<p>Diseñado por <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
			</div>
		</div>		
	</div>

	<script src="{{ URL::asset('js/classie.js') }}"></script>
	<script>
		var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
			showLeftPush = document.getElementById( 'showLeftPush' ),
			body = document.body;
			
		showLeftPush.onclick = function() {
			classie.toggle( this, 'active' );
			classie.toggle( body, 'cbp-spmenu-push-toright' );
			classie.toggle( menuLeft, 'cbp-spmenu-open' );
			disableOther( 'showLeftPush' );
		};
		
		function disableOther( button ) {
			if( button !== 'showLeftPush' ) {
				classie.toggle( showLeftPush, 'disabled' );
			}
		}
	</script>
   <script src="{{ URL::asset('js/bootstrap.min.js') }}"> </script>
   <script src="{{ URL::asset('js/script.js') }}"></script>
</body>
</html>