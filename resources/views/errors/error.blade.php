@extends('layout.pusaqruna', ['usuario' => session('usuario')])
@section('contenido')
<div class="error-info">
	<img src="{{ URL::asset('img/error/'.$codigo.'.png') }}" title="Error" />
	<p><span>Ohh ... </span>La página que solicitaste no está disponible</p>
</div>
@stop