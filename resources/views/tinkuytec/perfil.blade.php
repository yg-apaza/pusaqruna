@extends('layout.tinkuytec', ['usuario' => session('usuario')])

@section('contenido')

<div class="perfil container-fluid">
	<div class="content-perfil col-md-12" style="background-color: #fff">
		<div class="col-md-3 col-sm-6" style="padding: 1em">
			<center>
				<img src="{{ session('urlAvatar') }}" width=250px /><br><br>
				<button type="button" class="btn btn-default"><i class="fa fa-repeat"></i> Cambiar contraseña</button>
			</center>
		</div>
		<div class="col-md-9 col-sm-6" style="padding: 1em">
			<div class="but_list">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
						<li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Mis datos</a></li>
						<li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Datos adicionales</a></li>
					</ul>

					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
							<div class="table-responsive">
								<form method="get">
									<table class="table table-hover">
									    <tbody>
											<tr>
												<td>DNI:</td>
												<td><input type="text" autocomplete="off" class="required form-control" alt="Número de DNI" name="dni" value="{{ session('usuario')['dni'] }}" disabled></td>
											</tr>
											<tr>
												<td>Nombres:</td>
												<td><input type="text" autocomplete="off" class="required form-control" alt="Nombres" name="nombres" value="{{ session('usuario')['nombres'] }}" disabled></td>
											</tr>
											<tr>
												<td>Apellidos:</td>
												<td><input type="text" autocomplete="off" class="required form-control" alt="Apellidos" name="apellidos" value="{{ session('usuario')['apellidos'] }}" disabled></td>
											</tr>
											<tr>
												<td>Correo electrónico:</td>
												<td><input type="text" autocomplete="off" class="required form-control" alt="Correo electrónico" name="correo" value="{{ session('usuario')['correo'] }}" disabled></td>
											</tr>
									    </tbody>
									</table>
									<center>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Modificar</button>
										<button type="submit" class="btn btn-default" disabled><i class="fa fa-save"></i> Guardar cambios</button>
									</center>
								</form>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
							<div class="table-responsive">
								<form method="get">
									<table class="table table-hover">
									    <tbody>
											<tr>
												<td>Profesión:</td>
												<td><input type="text" autocomplete="off" class="required form-control" alt="Profesión" name="profesion" value="No especificado" disabled></td>
											</tr>
											<tr>
												<td>Especialidad:</td>
												<td><input type="text" autocomplete="off" class="required form-control" alt="Especialidad" name="especialidad" value="No especificado" disabled></td>
											</tr>
											<tr>
												<td>Institución de trabajo:</td>
												<td><input type="text" autocomplete="off" class="required form-control" alt="Institución" name="institucion" value="No especificado" disabled></td>
											</tr>
											<tr>
												<td>Teléfono:</td>
												<td><input type="text" autocomplete="off" class="required form-control" alt="Teléfono" name="telefono" value="No especificado" disabled></td>
											</tr>
											<tr>
												<td>Departamento:</td>
												<td><select name="dpto" disabled>
													<option value="0">No especificado</option>
													<option value="1">Amazonas</option>
													<option value="2">Ancash</option>
													<option value="3">Apurimac</option>
													<option value="4">Arequipa</option>
													<option value="5">Ayacucho</option>
													<option value="6">Cajamarca</option>
													<option value="7">Cusco</option>
													<option value="8">Huancavelica</option>
													<option value="9">Huanuco</option>
													<option value="10">Ica</option>
													<option value="11">Junin</option>
													<option value="12">La Libertad</option>
													<option value="13">Lambayeque</option>
													<option value="14">Lima</option>
													<option value="15">Loreto</option>
													<option value="16">Madre De Dios</option>
													<option value="17">Moquegua</option>
													<option value="18">Pasco</option>
													<option value="19">Piura</option>
													<option value="20">Puno</option>
													<option value="21">San Martin</option>
													<option value="22">Tacna</option>
													<option value="23">Tumbes</option>
													<option value="24">Ucayali</option>
												</select></td>
											</tr>
									    </tbody>
									</table>
									<center>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Modificar</button>
										<button type="submit" class="btn btn-default"><i class="fa fa-save"></i> Guardar cambios</button>
									</center>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row" style="padding: 1em">
		<hr>
		<div class="col-xs-12 col-sm-6 col-md-8">
			<a name="medallas"></a> 
			<h3>Mis Medallas</h3>
			<br>
			<p class="text-center" style="color: #aaa">Usted no posee ninguna medalla. Culmine cursos para obtener más medallas.</p>
			<hr>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<h3>Mi Kallpa: {{ session('usuario')["kallpa"] }}</h3>
			<div class="row">
				<div class="col-md-6">
					<div class="avatar-container p-0">
		            	<img src="{{ URL::asset('img/nivel/1.png') }}" alt="" class="avatar"/>
	            	</div>
		        </div>
		        <div class="col-md-6">
		        	<p>Has acumulado <strong>{{ session('usuario')["kallpa"] }}</strong> horas académicas</p><br>
		        	<p>Nivel:
			        	<span class="badge badge-primary">
				        	@if(session('usuario')["kallpa"] <= 500) EXPERTO
				        	@elseif(session('usuario')["kallpa"] <= 1000) ERUDITO
				        	@elseif(session('usuario')["kallpa"] <= 3000) SABIO
				        	@elseif(session('usuario')["kallpa"] <= 10000) EMINENCIA
				        	@else GENIO
				        	@endif
			        	</span>
			        </p>
		        </div>
	        </div>
			<hr>
			<div class="clearfix">
				<h3>Mis cursos</h3>
				<a href="{{ URL::asset('tinkuytec/miscursos') }}" class="btn btn-success btn-md pull-right" role="button">Ver mis cursos</a>
			</div>
			<hr>
			<h3>Mis certificados</h3>
			<a href="{{ URL::asset('tinkuytec/miscertificados') }}" class="btn btn-success btn-md pull-right" role="button">Ver mis certificados</a>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<script src="{{ URL::asset('js/index.js') }}"></script>
@stop