@extends('layout.tinkuytec', ['usuario' => session('usuario')])

@section('contenido')
<div class="container-fluid">
	<div class="courses_box1">
		<div class="col-md-6 about_left">
			<p>Tinkuy.TEC  pone a disposición de cualquier interesado <strong>Cursos Online Masivos en Abierto (más conocidos como MOOC's)</strong> a través de una plataforma abierta sin restricciones, sin condiciones, sin horarios, SIN BARRERAS.</p>
			<br>
			<p>La iniciativa la promovemos:</p>
			<ul class="about_links">
				<li><a href="#"><strong>Pusaq Runa SAC</strong>, consultora especializada en ofrecer soluciones integrales de aprendizaje online para la Educación y Formación.</a></li>
			</ul>
			<a href="#mas" class="radial_but">Leer más</a>
		</div>
		<div class="col-md-6">
			<div class="video-container">
				<iframe width="600" height="450" src="https://www.youtube.com/embed/S_F7Bqwj0qw?modestbranding=1" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
	<div class="bg_color">
		<div class="container-fluid">
			<div class="col-md-6 service_2-left">
		    	<h2>NUESTRA FILOSOFÍA</h2>
		 	</div>
			<div class="col-md-6 service_2-right">
				<p>Con Tinkuy.TEC ponemos a disposición de nuestros colaboradores y partner un espacio en el que puedan transmitir conocimiento de forma libre, para fomentar entre todos el intercambio de experiencias e ideas en torno al mismo entre quienes lo reciben a través de la red.</p>
				<br>
				<p>Desde Tinkuy.TEC, junto a las instituciones y empresas participantes, queremos ofrecerte cursos de muy diversas temáticas, diseñados para que puedas seguirlos a tu ritmo, en los que puedas aprender desde cero sobre las materias que te interesan, profundizar en ellas si ya tenías conocimientos previos y que te ayuden a desarrollar habilidades o a potenciarlas.</p>
			</div>
		 	<div class="clearfix"> </div>
	 	</div>
	</div>
<a name="mas"></a>

<div class="services">
	<div class="container-fluid">
		<h1>MÁS</h1>
		<div class="service_box1">
			<div class="col-md-6">
				<div class="service_1">
				    <div class="service_1-left">
						<span class="icon_5"><i class="fa fa-users"> </i></span>
				    </div>
				    <div class="service_1-right">
				       	<h5><a href="#">Conoce a quién nos dirigimos</a></h5>
				       	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			                <div class="panel panel-default">
			                    <div class="panel-heading" role="tab" id="headingOne">
			                        <h4 class="panel-title">
			                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			                               <i class="fa fa-bookmark-o icon_3"></i>¿Interesado en crear e impartir cursos?
			                            </a>
			                        </h4>
			                    </div>
			                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true">
			                        <div class="panel-body">
			                            Tinkuy.TEC está a disposición de los especialistas de nuestras empresas e instituciones partner. Si quieres saber qué tienes que hacer para crear e impartir un curso en Tinkuy.TEC, solicítanos información <a href="{{ URL::asset('/tinkuytec/unete') }}">aquí</a>.
			                        </div>
			                    </div>
			                </div>
			                <div class="panel panel-default">
			                    <div class="panel-heading" role="tab" id="headingTwo">
			                        <h4 class="panel-title">
			                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			                                <i class="fa fa-pencil-square-o icon_3"></i>¿Interesado en inscribirte y participar en cursos?
			                            </a>
			                        </h4>
			                    </div>
			                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
			                        <div class="panel-body">
			                        	Tinkuy.TEC está a disposición de cualquier persona interesada en sus cursos. ¡Eso es todo! No tienes que ser alumno, o egresado de ninguna universidad, algunos cursos son gratuitos y sólo debes pagar el costo de la certificación, ni solicitar admisiones puesto que no tienen límite de alumnos, ni descargar ningún software específico para tu ordenador, ni adaptarte a horarios porque están diseñados para que sigas los contenidos y hagas las actividades cuando puedas y quieras. ¡Sólo tienes que inscribirte y empezar cuando llegue su fecha de inicio!
			                        </div>
			                    </div>
			                </div>
			            </div>
				    </div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="service_1">
				    <div class="service_1-left">
				       	<span class="icon_5"><i class="fa fa-gear"> </i></span>
				    </div>
				    <div class="service_1-right">
				        <h5><a href="#">Conoce cómo funciona</a></h5>
				        <p>En nuestra breve experiencia, hemos detectado que pueden surgir de forma frecuente una serie de dudas antes, durante y después del curso. Consúltalas <!--a href="./faq"--><a href="{{ URL::asset('normastrabajo') }}">aquí</a>.</p>
				    </div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="service_box1">
		    <div class="col-md-6">
		        <div class="service_1">
		            <div class="service_1-left">
		                <span class="icon_5"><i class="fa fa-check-square-o"> </i></span>
		            </div>
		            <div class="service_1-right">
		                <h5><a href="#">Conoce lo que podemos ofrecerte</a></h5>
		                <p>Si quieres conocer los cursos que tenemos disponibles en el momento, puedes consultar cuando estés navegando por la plataforma el buscador o el <a href="{{ URL::asset('tinkuytec/cursos') }}">canal de cursos</a>.</p>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-6">
		        <div class="service_1">
		            <div class="service_1-left">
		                <span class="icon_5"><i class="fa fa-share"> </i></span>
		            </div>
		            <div class="service_1-right">
		                <h5><a href="#">Conoce dónde seguirnos</a></h5>
		                <p>Además de nuestro blog, en el que publicamos asiduamente información sobre nuestros cursos y nuestra plataforma, puedes seguirnos en los diferentes canales sociales como Twitter, Facebook, Linkedin y Google+.</p>
		            </div>
		        </div>
		    </div>
		    <div class="clearfix"> </div>
		</div>
	</div>
</div>
@stop