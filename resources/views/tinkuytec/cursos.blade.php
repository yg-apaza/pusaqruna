@extends('layout.tinkuytec', ['usuario' => session('usuario')])

@section('contenido')
<div class="courses_box1">
    <div class="container-fluid">
        <div class="col-md-12 detail">
            @if (session('success'))
                <div class="alert alert-success fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Éxito !</strong><br>
                    {{ session('success') }}
                </div>
            @endif
            <form>
                <div class="col-md-4 detail">
                    <div class="select-block1">
                        <select name="cat">
                            <option value="0">Todas las categorías</option>
                            <option value="1">Ed. Inicial</option>
                            <option value="2">Ed. Primaria</option>
                            <option value="3">Ed. Secundaria</option>
                            <option value="4">Ed. Intercultural Bilingüe</option>
                            <option value="5">Ed. Alternativa Básica</option>
                            <option value="6">Tutoría</option>
                            <option value="7">Tecnología e Innovación</option>
                            <option value="8">Robótica Educativa</option>
                            <option value="9">Gestión y Dirección</option>
                            <option value="10">Animación a la lectura</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 detail">
                    <div class="select-block1">
                        <select name="tipo">
                            <option value="0">Todo tipo</option>
                            <option value="1">Gratuito</option>
                            <option value="2">De pago</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 detail">
                    <div class="select-block1">
                        <input type="text" autocomplete="off" placeholder="Ingrese un curso ..." name="q" value="">
                    </div>
                </div>
                <div class="col-md-2 detail">
                    <div class="select-submit">
                        <input type="submit" value="BUSCAR" class="course-submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid courses-list">
        <div class="col-md-12 detail course-item">
            <div class="row">
                <div class="col-xs-5 col-sm-5">
                    <div class="event-img">
                        <img src="{{ URL::asset('img/cursos/1.jpg') }}" class="img-responsive" alt=""/>
                        <div class="over-image"></div>
                    </div>
                </div>
                <div class="col-xs-7 col-sm-7 event-desc">
                    <h2><b>GOOGLE DRIVE: HERRAMIENTAS COLABORATIVAS EN EDUCACION</b></h2>
                    <div class="event-info-text">
                        <div class="event-info-middle">
                            <p style="display:inline;">Este curso es : <span class="badge badge-success">GRATIS</span></p>
                            <p>Fecha de inicio : 25 de enero del 2016</p><br>
                            <p><span class="course-bold">¿Quieres aprender a obtener beneficios didácticos de esta suite ofimática, sus aplicaciones y herramientas de forma online? Bienvenido al curso</span></p><br>
                            <a class="shortcode_but large" href="{{ URL::asset('tinkuytec/google-drive') }}" target="_self" style="color:#ffffff; background-color:#d64f4f; ">IR AL CURSO</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--div class="col-md-12 detail">
            <ul class="pagination event_pagination">
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
            </ul>
        </div-->
    </div>
    <div class="clearfix"> </div>
</div>
@stop