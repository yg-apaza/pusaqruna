@extends('layout.tinkuytec', ['usuario' => session('usuario')])

@section('contenido')
<div class="container-fluid">
	@if(session('info'))
	<div class="alert alert-info fade in">
	    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
	    <strong>Información</strong><br>
	    {{ session('info') }}
	</div>
	@endif
</div>
<div class="banner">
	<!-- Banner Slider -->
	<script src="{{ URL::asset('js/responsiveslides.min.js') }}"></script>
	<script>
		$(function () {
			$("#slider3").responsiveSlides({
				auto: true,
				pager: true,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>
	
	<div  id="top" class="callbacks_container">
		<ul class="rslides" id="slider3">
			<li>
				<div class="banner-bg banner-img2">
					<div class="banner-info">
						<h3>TINKUY.TEC</h3>
						<p>Es una plataforma de aprendizaje e-Learning, la cual es una solución específicamente diseñada y concebida para todos los niveles de conocimiento y necesidades educativas
						</p>
						<a href="@if(session('usuario'))
									{{ URL::asset('tinkuytec/cursos') }}
								 @else
									{{ URL::asset('tinkuytec/registro') }}
								 @endif"><i class="fa fa-thumbs-up icon_1" style="font-size: 20px; transition: color 0.2s ease 0s, border-color 0.2s ease 0s, background-color 0.2s ease 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 0px 2px 0px 0px; margin: 0px; padding:0px 10px 0 0; letter-spacing: 0px;"></i>Empieza ahora</a>
					</div>
				</div>
			</li>
			<li>
				<div class="banner-bg banner-img1">
					<div class="banner-info">
						<h3>CREANDO UN MUNDO SIN FRONTERAS</span></h3>
						<p>Pusaq Runa SAC apuesta por impulsar el conocimiento abierto de Educación Superior en el ámbito nacional, un conocimiento que es transmitido y enriquecido a través de la red
						</p>
						<a href="@if(session('usuario'))
									{{ URL::asset('tinkuytec/cursos') }}
								 @else
									{{ URL::asset('tinkuytec/registro') }}
								 @endif"><i class="fa fa-thumbs-up icon_1" style="font-size: 20px; transition: color 0.2s ease 0s, border-color 0.2s ease 0s, background-color 0.2s ease 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 0px 2px 0px 0px; margin: 0px; padding:0px 10px 0 0; letter-spacing: 0px;"></i>Empieza ahora</a>
					</div>
				</div>
			</li>
			<li>
				<div class="banner-bg banner-img3">
					<div class="banner-info">
						<h3>CURSOS DISPONIBLES</h3>
						<p>Ofrecemos un amplio abanico de metodologías y servicios educativos basados en la investigación permanente, estudios pedagógicos y el desarrollo de nuevos productos.
						</p>
						<a href="@if(session('usuario'))
									{{ URL::asset('tinkuytec/cursos') }}
								 @else
									{{ URL::asset('tinkuytec/registro') }}
								 @endif"><i class="fa fa-thumbs-up icon_1" style="font-size: 20px; transition: color 0.2s ease 0s, border-color 0.2s ease 0s, background-color 0.2s ease 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 0px 2px 0px 0px; margin: 0px; padding:0px 10px 0 0; letter-spacing: 0px;"></i>Empieza ahora</a>
					</div>
				</div>
			</li>
		</ul>
    </div>
</div>

<div class="bg_color">
	<div class="container-fluid">
		<div class="col-md-6 service_2-left">
	    	<h2>¿QUÉ ES UNA PLATAFORMA E-LEARNING?</h2>
	 	</div>
		<div class="col-md-6 service_2-right">
			<p>Es un entorno virtual de enseñanza y aprendizaje. Es decir, una aplicación web que integra un conjunto de herramientas para la enseñanza en línea, permitiendo el aprendizaje no presencial o virtual (e-learning).</p>
			<br>
			<p>El objetivo principal de una plataforma e-Learning es crear y gestionar espacios de enseñanza y aprendizaje en Internet, donde los profesores y los estudiantes puedan interactuar durante su proceso de formación.</p>
		</div>
	 	<div class="clearfix"> </div>
 	</div>
</div>

<!--div class="grid_1">
	<div class="col-md-4">
		<div class="news">
			<h1>ARTÍCULOS RECIENTES</h1>
			<a href="{{ URL::asset('blog') }}" class="read-more">Ver todos los artículos</a>
			<br><br>
			<div class="section-content">
				<article>
				    <figure class="date"><i class="fa fa-file-o"></i>16-01-2016</figure>
				    <a href="#">¿Quieres formarte por tu cuenta?</a>
				</article>
				<article>
				    <figure class="date"><i class="fa fa-file-o"></i>16-01-2016</figure>
				    <a href="#">España continúa siendo líder en la generación de cursos MOOC.</a>
				</article>
				<article>
				    <figure class="date"><i class="fa fa-file-o"></i>11-01-2016</figure>
				    <a href="#">Juan Medina: "No es difícil preparar un MOOC".</a>
				</article>
				<article>
				    <figure class="date"><i class="fa fa-file-o"></i>29-12-2015</figure>
				    <a href="#">Encontrando tesoros en la red.</a>
				</article>
			</div>
		</div>
	</div>

	<div class="col-md-8 grid_1_right">

		<h2>ÚLTIMOS CURSOS</h2>
		<a href="{{ URL::asset('cursos') }}" class="read-more">Ver todos los cursos</a>
		<br><br>
		<div class="but_list">
			<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
					<li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">1</a></li>
				</ul>

				<div id="myTabContent" class="tab-content">

					<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
						<div class="events_box">
							<div class="event_left">
								<div class="event_left-item">
									<div class="icon_2"><i class="fa fa-location-arrow"></i><span class="badge badge-success">GRATIS</span></div>
									<div class="icon_2"><i class="fa fa-user"></i>Mg. Fabiola Frisancho Ramírez</div>
									<div class="icon_2">
									  	<div class="speaker">
										  	<div class="speaker_item">
										    	<a class="shortcode_but medium" href="{{ URL::asset('curso/google-drive') }}" target="_self" style="color:#ffffff; background-color:#d64f4f; ">IR AL CURSO</a>
										  	</div>
										  	<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="event_right">
								<h3><a href="{{ URL::asset('curso/google-drive') }}">GOOGLE DRIVE: HERRAMIENTAS COLABORATIVAS EN EDUCACION</a></h3>
								<p>¿Quieres aprender a obtener beneficios didácticos de esta suite ofimática, sus aplicaciones y herramientas de forma online? Bienvenido al curso</p>
								<img src="{{ URL::asset('img/cursos/c01.jpg') }}" class="img-responsive"/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="but_list">
			<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
					<li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">1</a></li>
					<li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">2</a></li>
					<li role="presentation"><a href="#profile1" role="tab" id="profile-tab1" data-toggle="tab" aria-controls="profile1">3</a></li>
					<li role="presentation"><a href="#profile2" role="tab" id="profile-tab2" data-toggle="tab" aria-controls="profile2">4</a></li>
				</ul>

				<div id="myTabContent" class="tab-content">

					<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
						<div class="events_box">
							<div class="event_left">
								<div class="event_left-item">
									<div class="icon_2"><i class="fa fa-location-arrow"></i><span class="badge badge-success">GRATIS</span></div>
									<div class="icon_2"><i class="fa fa-user"></i>Mg. Fabiola Frisancho Ramírez</div>
									<div class="icon_2">
									  	<div class="speaker">
										  	<div class="speaker_item">
										    	<a class="shortcode_but medium" href="{{ URL::asset('registro') }}" target="_self" style="color:#ffffff; background-color:#d64f92; ">Inscribirse</a>
										  	</div>
										  	<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="event_right">
								<h3><a href="#">ROBÓTICA EDUCATIVA</a></h3>
								<p>La robótica educativa es un medio de aprendizaje, en el cual participan las personas que tienen motivación por el diseño y construcción de creaciones propias. Estas creaciones se dan, en primera instancia, de forma mental y, posteriormente, en forma física, y son construidas con diferentes tipos de materiales, y controladas por un sistema computacional, los que son llamados prototipos o simulaciones</p>
								<img src="{{ URL::asset('img/cursos/c01.jpg') }}" class="img-responsive"/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
						<div class="events_box">
							<div class="event_left">
								<div class="event_left-item">
									<div class="icon_2"><i class="fa fa-location-arrow"></i><span class="badge badge-success">GRATIS</span></div>
									<div class="icon_2"><i class="fa fa-user"></i>Mg. Fabiola Frisancho Ramírez</div>
									<div class="icon_2">
									 	<div class="speaker">
										  	<div class="speaker_item">
										    	<a class="shortcode_but medium" href="{{ URL::asset('registro') }}" target="_self" style="color:#ffffff; background-color:#d64f92; ">Inscribirse</a>
										  	</div>
										  	<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="event_right">
								<h3><a href="#">CREACIÓN DE HISTORIETAS DIGITALES</a></h3>
								<p>Son historietas en formato digital, para ser visualizado en los PC o cualquier dispositivo que posea una pantalla y memoria, como una videoconsola o un teléfono móvil, adecuado para este propósito.</p>
								<img src="{{ URL::asset('img/cursos/c02.jpg') }}" class="img-responsive"/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane fade" id="profile1" aria-labelledby="profile-tab1">
						<div class="events_box">
							<div class="event_left">
								<div class="event_left-item">
									<div class="icon_2"><i class="fa fa-location-arrow"></i><span class="badge badge-success">GRATIS</span></div>
									<div class="icon_2"><i class="fa fa-user"></i>Mg. Fabiola Frisancho Ramírez</div>
									<div class="icon_2">
									    <div class="speaker">
											<div class="speaker_item">
												<a class="shortcode_but medium" href="{{ URL::asset('registro') }}" target="_self" style="color:#ffffff; background-color:#d64f92; ">Inscribirse</a>
											</div>
										  	<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="event_right">
								<h3><a href="#">ELABORACIÓN DE BLOGS</a></h3>
								<p>Un blog es un sitio Web en donde uno o varios autores desarrollan contenidos. Los blog también se conocen como weblog o cuaderno de bitácora. La información se actualiza periódicamente y, de la misma forma, los textos se plasman en forma cronológica; primero aparece el más recientemente escrita.</p>
								<img src="{{ URL::asset('img/cursos/c03.jpg') }}" class="img-responsive"/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane fade" id="profile2" aria-labelledby="profile-tab2">
						<div class="events_box">
							<div class="event_left">
								<div class="event_left-item">
									<div class="icon_2"><i class="fa fa-location-arrow"></i><span class="badge badge-success">GRATIS</span></div>
									<div class="icon_2"><i class="fa fa-user"></i>Mg. Fabiola Frisancho Ramírez</div>
									<div class="icon_2">
									  	<div class="speaker">
										  	<div class="speaker_item">
										    	<a class="shortcode_but medium" href="{{ URL::asset('registro') }}" target="_self" style="color:#ffffff; background-color:#d64f92; ">Inscribirse</a>
										  	</div>
										  <div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="event_right">
								<h3><a href="#">BÚSQUEDA Y ORGANIZACIÓN DE LA INFORMACIÓN</a></h3>
								<p>Es el conjunto de operaciones o tareas que tienen por objeto poner al alcance de un usuario la información que de respuesta a sus preguntas, mediante la localización y acceso a los recursos de información pertinentes.</p>
								<img src="{{ URL::asset('img/cursos/c04.jpg') }}" class="img-responsive"/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div-->
@stop