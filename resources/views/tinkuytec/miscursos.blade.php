@extends('layout.tinkuytec', ['usuario' => session('usuario')])

@section('contenido')
<div class="admission">
   <div class="container-fluid">
   		<p>A continuación te presentamos todos los cursos en los que estás inscrito, accede a ellos para revisar los temas y actividades nuevas, también puedes utilizar el buscador: </p>
   	 	<div class="col-md-12 detail">
            <form>
                <div class="col-md-4 detail">
                    <div class="select-block1">
                        <select name="cat">
                            <option value="0">Todas las categorías</option>
                            <option value="1">Ed. Inicial</option>
                            <option value="2">Ed. Primaria</option>
                            <option value="3">Ed. Secundaria</option>
                            <option value="4">Ed. Intercultural Bilingüe</option>
                            <option value="5">Ed. Alternativa Básica</option>
                            <option value="6">Tutoría</option>
                            <option value="7">Tecnología e Innovación</option>
                            <option value="8">Robótica Educativa</option>
                            <option value="9">Gestión y Dirección</option>
                            <option value="10">Animación a la lectura</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 detail">
                    <div class="select-block1">
                        <input type="text" autocomplete="off" placeholder="Ingrese un curso ..." name="q" value="">
                    </div>
                </div>
                <div class="col-md-2 detail">
                    <div class="select-submit">
                        <input type="submit" value="BUSCAR" class="course-submit">
                    </div>
                </div>
            </form>
        </div>
   	 	<div class="faculty_top">
   	  		<div class="col-md-4 faculty_grid micurso">
   	  			<figure class="team_member">
	   	  			<img src="{{ URL::asset('img/cursos/1.jpg') }}" class="img-responsive wp-post-image" width=100% height=100%/>
	   	  			<div></div>
		   	  		<figcaption><h3 class="person-title">
		   	  			<a href="{{ URL::asset('tinkuytec/google-drive') }}">GOOGLE DRIVE: HERRAMIENTAS COLABORATIVAS EN EDUCACION</a></h3>
		   	  			<div class="person-social">
		   	  				<a class="shortcode_but small" href="{{ URL::asset('tinkuytec/google-drive') }}" target="_self" style="color:#ffffff; background-color:#d64f4f; ">Ir al curso</a>
		   	  		  	</div>
		   	  	   	</figcaption>
   	  			</figure>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                    </div>
                    <p class="text-center">0% completo</p>
                </div>
    		</div>
	   	  	<div class="clearfix"> </div>
	 	</div>
  	</div>
</div>
@stop