@extends('layout.tinkuytec', ['usuario' => session('usuario')])

@section('contenido')
<div class="container-fluid">
    <div class="courses_box1">
        @if(session('info'))
        <div class="alert alert-info fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <strong>Información</strong><br>
            {{ session('info') }}
        </div>
        @endif
        @if (count(session('errores')) > 0)
            <div class="alert alert-danger fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong>Errores encontrados</strong><br>
                @foreach (session('errores') as $e)
                    {{ $e }} <br>
                @endforeach
            </div>
        @endif
	    <form class="login" method="post">
            <p class="lead">REGÍSTRATE GRATIS !</p>
            <div class="form-group">
                <input type="text" autocomplete="off" class="required form-control" alt="Número de DNI" placeholder="DNI" name="dni" value="{{ session('recover')[0] }}">
            </div>
            <div class="form-group">
                <input type="text" autocomplete="off" class="required form-control" alt="Nombres" placeholder="Nombres" name="nombres" value="{{ session('recover')[1] }}">
            </div>
            <div class="form-group">
                <input type="text" autocomplete="off" class="required form-control" alt="Apellidos" placeholder="Apellidos" name="apellidos" value="{{ session('recover')[2] }}">
            </div>
            <div class="form-group">
                <input type="text" autocomplete="off" class="required form-control" alt="Correo electrónico" placeholder="Correo electrónico" name="correo" value="{{ session('recover')[3] }}">
            </div>
            <div class="form-group">
                <input type="password" class="required form-control" alt="Contraseña" placeholder="Contraseña" name="password" value="">
            </div>
            <div class="form-group g-recaptcha" data-sitekey="{{ Config::get('g_recaptcha.site_key') }}"></div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-lg1 btn-block" value="Registrar">
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <p>¿Ya tienes una cuenta? <a href="{{ URL::asset('tinkuytec/acceso') }}">Accede</a></p>
        </form>
    </div>
</div>
@stop