@extends('layout.tinkuytec', ['usuario' => session('usuario')])

@section('contenido')
<div class="container-fluid">
    <div class="courses_box1">
        @if (session('success'))
            <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong>Éxito !</strong><br>
                {{ session('success') }}
            </div>
        @endif
        
        @if (count(session('errores')) > 0)
            <div class="alert alert-danger fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong>Errores encontrados</strong><br>
                @foreach (session('errores') as $e)
                    {{ $e }} <br>
                @endforeach
            </div>
        @endif
        @if(session('info'))
            <div class="alert alert-info fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <strong>Información</strong><br>
                {{ session('info') }}
            </div>
        @endif
        <form class="login" method="post">
            <p class="lead">BIENVENIDO DE NUEVO !</p>
            <div class="form-group">
                <input autocomplete="off" type="text" name="correo" class="required form-control" placeholder="Correo electrónico" value="{{ session('recover') }}">
            </div>
            <div class="form-group">
                <input autocomplete="off" type="password" class="password required form-control" placeholder="Contraseña" name="password">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-lg1 btn-block" name="submit" value="Entrar">
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <p>¿No tienes una cuenta? <a href="{{ URL::asset('tinkuytec/registro') }}">Regístrate</a></p>
        </form>
    </div>
</div>
@stop