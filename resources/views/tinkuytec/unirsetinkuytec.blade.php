@extends('layout.tinkuytec', ['usuario' => session('usuario')])

@section('contenido')

<div class="courses_box1">
	<div class="container-fluid">
		<h2>¿Cómo publicar un curso en Tinkuy.TEC?</h2>
		<br>
		<div class="col-md-6 about_left">
			<p>Si eres docente de EBR, EIB, EBA o superior en cualquier carrera profesional y estás interesado en participar en la iniciativa publicando un curso, contacta con el equipo de Tinkuy.TEC responsable del proyecto y te facilitarán toda la información sobre cómo crear e impartir cursos en Tinkuy.TEC. ¡No dudes en ponerte en contacto!</p>
			<br>
			<div class="list-group list-group-alternate"> 
				<a class="list-group-item"><span class="badge badge-primary">tinkuy.tec.sur@pusaqruna.com</span>
					<strong>SUR</strong>
					<br>
					Arequipa – Tacna – Moquegua – Puno – Cusco – Madre de Dios – Apurímac - Ayacucho
				</a> 
				<a class="list-group-item"><span class="badge badge-primary">tinkuy.tec.centro@pusaqruna.com</span>
					<strong>CENTRO</strong>
					<br>
					Lima – Ica – Huancavelica – Junín – Ucayali – Pasco – Huánuco - Ancash
				</a> 
				<a class="list-group-item"><span class="badge badge-primary">tinkuy.tec.norte@pusaqruna.com</span>
					<strong>NORTE</strong>
					<br>
					La Libertad – San Martín – Loreto – Amazonas – Cajamarca – Lambayeque – Piura - Tumbes
				</a> 
			</div>
		</div>
		<div class="col-md-6">
			<div class="video-container">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/i3CmZBEw0_g?modestbranding=1" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
@stop