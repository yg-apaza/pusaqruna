@extends('layout.tinkuytec', ['usuario' => session('usuario')])

@section('contenido')
<div class="courses_box1">
    <div class="container-fluid">
        <h2>Antes de comenzar debes saber que …</h2>
        <br>
        <div class="bg_normas">
            <div class="col-md-12 service_2-right">
                <p>Los cursos ofrecidos por las instituciones y empresas en <strong>Tinkuy.TEC</strong> están diseñados para ofrecer la misma calidad y rigor que ofrece un curso presencial. A continuación te presentamos los aspectos más importantes para la realización y superación de los cursos que te permitirán sacar el máximo provecho de tu experiencia en <strong>Tinkuy.TEC</strong>:</p>
            </div>
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                           <i class="fa fa-pencil icon_3"></i>1. Actividades obligatorias y optativas
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true">
                    <div class="panel-body">
                        Los cursos constan de actividades obligatorias y optativas. Los docentes seleccionan qué tareas hay que realizar obligatoriamente para superar el curso y poder optar a los diplomas y badges. Revisa el syllabus de cada curso para conocer las actividades obligatorias de cada módulo y así poder organizar tu trabajo. Independientemente de los diplomas y badges, te recomendamos hacer el mayor número de actividades posible, incluyendo las optativas, para tener la mejor experiencia de aprendizaje.
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <i class="fa fa-certificate icon_3"></i>2. Diplomas y badges
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        Actualmente existen dos modalidades de diploma y badge:
                        <br><br>
                        <ul class="marked-list">
                            <li>El <strong>certificado de participación</strong> se libera automáticamente cuando se supera un promedio del 75% de todas las actividades obligatorias que hayas realizado.</li>
                            <li>El <strong>certificado de superación</strong> podrás solicitarlo cuando hayas superado la totalidad de las actividades obligatorias incluidas en el curso.</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <i class="fa fa-clock-o icon_3"></i>3. Fechas límite
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        Si te has inscrito en algún curso el contenido del mismo siempre estará disponible para tu consulta. Sin embargo, algunas de las actividades de evaluación, normalmente obligatorias, tienen fechas límite para su realización. Revisa atentamente estas fechas en el syllabus de cada curso puesto que <strong class="advert">una vez superada la fecha límite de cada tarea, no podrás realizar ni superar dicha tarea</strong> impidiendo que consigas los diplomas y badges del curso.
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <i class="fa fa-exchange icon_3"></i>4. Actividades P2P o evaluación entre iguales
                        </a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        Son actividades muy destacadas de los cursos puesto que en ellas no solo eres responsable de tu propio trabajo, sino que además debes evaluar los trabajos de otros de tus compañeros. Generalmente, estas actividades son actividades obligatorias por lo que tendrás que planificarlas bien. Ten en cuenta que <strong class="advert">las actividades P2P tienen dos fechas límite</strong>:
                        <br><br>
                        <ul class="marked-list">
                            <li>La primera fecha límite para entregar tu actividad.</li>
                            <li>La segunda fecha límite para validar las actividades de tus compañeros.</li>
                        </ul>
                        <br>
                        Recibirás un correo electrónico confirmando la entrega de la actividad indicándote los pasos a seguir. Conserva estos correos electrónicos para tener constancia de que has entregado las actividades. Aunque estas son las pautas generales de realización de cursos en Tinkuy.TEC, te recomendamos que prestes atención a las indicaciones que cada equipo docente puede dar en cada uno de ellos.
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFour">
                            <i class="fa fa-star icon_3"></i>5. Compromiso de Honestidad
                        </a>
                    </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        Como estudiante de los cursos de la Plataforma Tinkuy.TEC y con objeto de participar en los mismos cumpliendo con los principios que rigen su actividad educativa, principios de justicia, igualdad y excelencia, asumo los siguientes compromisos:
                        <br><br>
                        <ul class="marked-list">
                            <li>No registrarme en Tinkuy.TEC con más de una cuenta.</li>
                            <li>Que las respuestas a las actividades de tipo test y actividades de tipo P2P sean fruto de mi propio trabajo y la elaboración de las mismas se realice individualmente (excepto aquellas actividades en cuyo enunciado se permite explícitamente la colaboración).</li>
                            <li>No publicar ni facilitar en forma alguna las soluciones de las tareas de evaluación con la intención de beneficiar a otros estudiantes.</li>
                            <li>No llevar a cabo acciones que puedan mejorar de forma deshonesta los resultados de mis tareas de evaluación, ni mejorar o perjudicar las puntuaciones de otros estudiantes.</li>
                            <li>No reivindicar el trabajo de otros como propio.</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseFour">
                            <i class="fa fa-warning icon_3"></i>6. Netiqueta
                        </a>
                    </h4>
                </div>
                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                        Tinkuy.TEC es una comunidad en línea en la que cientos de alumnos participan en cada uno de sus cursos y están en constante comunicación a través de las herramientas sociales de la plataforma. Esta comunicación es escrita, lo cual implica algunas dificultades a la hora de transmitir sentimientos o ideas frente a la comunicación oral, en la que podemos usar gestos, modulaciones y expresiones que ayudan a los interlocutores a interpretar su lenguaje.
                        <br><br>
                        Para superar estas dificultades de comunicación, desde Tinkuy.TEC, queremos fomentar el uso de la Netiqueta, que consiste en una serie de pautas y recomendaciones de conducta a poner en práctica en la comunicación dentro de la comunidad y potenciar, de este modo, una comunicación más efectiva. A continuación puedes revisar las pautas de Netiqueta que en Tinkuy.TEC consideramos esenciales:
                        <br><br>
                        <ul class="marked-list">
                            <li>No olvidar que lo escrito es leído por personas que tienen sentimientos y que ciertos contenidos y formas de expresión pueden resultar ofensivos.</li>
                            <li>Ser breves, claros y concisos en las contribuciones ayuda a una comunicación más eficiente.</li>
                            <li>Ser conscientes de la audiencia. No todos los miembros de la comunidad tienen la misma cultura, conocimientos y habilidades, por lo que es conveniente adaptarse a los diferentes tipos de interlocutores.</li>
                            <li>La comunicación dentro de la comunidad es asíncrona. Hay que tener presente que, en la mayoría de ocasiones, no se obtendrá respuesta inmediata a los comentarios publicados.</li>
                            <li>Tener en cuenta las normas de ortografía, léxico y sintaxis a la hora de elaborar un mensaje así como releer lo escrito antes de enviarlo para comprobar que tiene sentido.</li>
                            <li>Respetar la privacidad de terceros.</li>
                            <li>No utilizar la comunidad para publicitar temas completamente ajenos a la materia del curso.</li>
                            <li>Citar la fuente de la que se obtiene la información (bibliografía, recursos web, etcétera) es imprescindible para que los interlocutores conozcan el fundamento y el origen de los argumentos que se exponen en la comunidad.</li>
                            <li>Todo el mundo comete errores. Es esencial, para mantener una comunidad respetuosa, ser comprensivos con las equivocaciones ajenas e informar cortésmente de los errores.</li>
                            <li>Mantener debates en un ambiente sano e instructivo, evitando las malas interpretaciones a consecuencia de por ejemplo:
                                <ul class="marked-list">
                                    <li>El uso de las mayúsculas.</li>
                                    <li>El empleo de abreviaturas que no sean de uso habitual.</li>
                                    <li>El manejo de lenguaje grosero o soez.</li>
                                </ul>
                            </li>
                            <li>Tratar de enriquecer con cada una de las aportaciones a la comunidad, evitando comentarios vacíos de significado o contenido.</li>
                            <li>A la hora de responder a algún mensaje, es conveniente dejar alguna cita del mismo para que se entienda a qué se está refiriendo y a quién va dirigido, si a una persona concreta o si interesa a todo el grupo.</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <br>
        <p>Estamos seguros de que estas pautas ayudarán a crear un entorno basado en el respeto dentro de la comunidad de cada curso y los invitamos a fomentar el uso de las mismas en Tinkuy.TEC.</p>
        <br>
        <div class="author-box">
            <div class="author-box-left"><img src="{{ URL::asset('img/netiqueta.png') }}" class="img-responsive" alt=""/></div>
            <div class="author-box-right">		
                <p>Para ahondar en la Netiqueta recomendamos leer:</p>
                <p><a href="http://www.galaxiagutenberg.com/media/72116/escribir_en_internet_web.pdf" target="_blank">Escribir en internet: Guía para los nuevos medios y las redes sociales, de Fundéu</a></p>
            </div>
            <div class="clearfix"> </div>
        </div>
    <div class="clearfix"></div>
    </div>
</div>
@stop