<?php

return [
	'token'	=> env('MOODLE_TOKEN', 'null'),
	'url'	=> env('MOODLE_URL', 'localhost'),
	'fun'		=> [
		'registrar_usuario'		=> 'local_wstinkuytec_create_user',
		'actualizar_usuario'	=> 'core_user_update_users'
	]
];