<?php

return [
	'site_key'	=> env('G_SITE_KEY', ''),
	'secret_key'	=> env('G_SECRET_KEY', ''),
];