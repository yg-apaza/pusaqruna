<?php

$plugin->version  = 2016030201;
$plugin->requires = 2010112400;
$plugin->cron     = 0;
$plugin->release = '1.0 (Build: 2016030201)';
$plugin->maturity = MATURITY_STABLE;
$plugin->component = 'local_wstinkuytec';
