<?php

/**
 * Plugin de Servicio Web para Tinkuy.TEC
 * @package     localwstinkuytec
 * @copyright   2016 PusaqRuna S.A.C.
 * @author      Yuliana Apaza
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 o posterior
 */

require_once($CFG->libdir . "/externallib.php");

class external_create_user extends external_api
{
    public static function create_user_parameters()
    {
        return new external_function_parameters([
            'usuario' => new external_single_structure([
                'dni'       => new external_value(PARAM_TEXT, 'Nro. de DNI', VALUE_DEFAULT, ''),
                'nombres'   => new external_value(PARAM_TEXT, 'Nombres', VALUE_DEFAULT, ''),
                'apellidos' => new external_value(PARAM_TEXT, 'Apellidos', VALUE_DEFAULT, ''),
                'correo'    => new external_value(PARAM_TEXT, 'Correo electrónico', VALUE_DEFAULT, ''),
                'password'  => new external_value(PARAM_TEXT, 'Contraseña', VALUE_DEFAULT, '')
            ])
        ]);
    }

    public static function create_user($usuario)
    {
        global $CFG, $DB;
        require_once($CFG->dirroot . "/user/lib.php");
        require_once($CFG->dirroot . "/user/profile/lib.php");
        require_once($CFG->dirroot . "/lib/weblib.php");

        $params = self::validate_parameters(self::create_user_parameters(), ['usuario' => $usuario]);

        // Parametro de retorno
        $response = [
            'error_flag' => false,
            'error_message' => []
        ];

        // Patrones
        $pattern = ['/^[\s]+/', '/[\s]+$/', '/[\s]+/'];
        $replace = ['', '', ' '];

        // Parametros de entrada
        $user = [];
        $user['idnumber'] = preg_replace($pattern, $replace, $params['usuario']['dni']);
        $user['firstname'] = strtoupper(preg_replace($pattern, $replace, $params['usuario']['nombres']));
        $user['lastname'] = strtoupper(preg_replace($pattern, $replace, $params['usuario']['apellidos']));
        $user['email'] = strtolower(preg_replace($pattern, $replace, $params['usuario']['correo']));
        $user['username'] = $user['email'];
        $user['password'] = $params['usuario']['password'];
        $user['auth']   = 'manual';
        $user['lang'] = $CFG->lang;
        $user['calendartype'] = $CFG->calendartype;
        $user['confirmed'] = false;
        $user['mnethostid'] = $CFG->mnet_localhost_id;

        // Validacion de campos de usuario
        if(!preg_match('/^[0-9]{8}$/', $user['idnumber']))
        {
            $response['error_flag'] = true;
            $response['error_message'][] = 'Nro. de DNI incorrecto.';
        }

        if(!preg_match('/^[A-Za-záéíóúñ]+([\s][A-Za-záéíóúñ]+)*$/', $user['firstname']))
        {
            $response['error_flag'] = true;
            $response['error_message'][] = 'Nombres incorrectos.';
        }

        if(!preg_match('/^[A-Za-záéíóúñ]+([\s][A-Za-záéíóúñ]+)*$/', $user['lastname']))
        {
            $response['error_flag'] = true;
            $response['error_message'][] = 'Apellidos incorrectos.';
        }

        if (!validate_email($user['email'])) {
            $response['error_flag'] = true;
            $response['error_message'][] = 'Correo electrónico inválido.';
        }

        if ($DB->record_exists('user', ['username' => $user['username'], 'mnethostid' => $CFG->mnet_localhost_id])) {
            $response['error_flag'] = true;
            $response['error_message'][] = 'El correo electrónico ya ha sido registrado con otra cuenta.';
        }

        if (!check_password_policy($user['password'], $e)) {
            $response['error_flag'] = true;
            $pattern = ['/<div>/', '/<\/div>/'];
            $replace = ['', ''];
            $response['error_message'][] = preg_replace($pattern, $replace, $e);
        }

        //Crear el usuario
        if(!$response['error_flag']) {

            $user['id'] = user_create_user($user, true, false);
            $user['profile_field_kallpa'] = '0';
            profile_save_data((object) $user);
            \core\event\user_created::create_from_userid($user['id'])->trigger();
        }

        return $response;
    }

    public static function create_user_returns()
    {
        return new external_single_structure([
            'error_flag'    => new external_value(PARAM_BOOL, 'Bandera de error'),
            'error_message' => new external_multiple_structure(
                new external_value(PARAM_RAW, 'Mensaje de error')
            )
        ]);
    }

    private static function send_confirmation_email()
    {
        
    }
}