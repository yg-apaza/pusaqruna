<?php

/**
 * Plugin de Servicio Web para Tinkuy.TEC
 * @package   localwstinkuytec
 * @copyright 2016 PusaqRuna S.A.C.
 * @author    Yuliana Apaza
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 o posterior
 */

$string['pluginname'] = 'Servicio Web Tinkuy.TEC';
