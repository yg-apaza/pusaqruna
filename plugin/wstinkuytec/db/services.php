<?php

/**
 * Plugin de Servicio Web para Tinkuy.TEC
 * @package   localwstinkuytec
 * @copyright 2016 PusaqRuna S.A.C.
 * @author    Yuliana Apaza
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 o posterior
 */

$functions = [
    'local_wstinkuytec_create_user' => [
        'classname'   => 'external_create_user',
        'methodname'  => 'create_user',
        'classpath'   => 'local/wstinkuytec/extcreateuser.php',
        'description' => 'Crea un usuario',
        'type'        => 'write',
    ]
];