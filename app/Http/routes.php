<?php

use PusaqRuna\Http\Controllers\Autorizador;
use PusaqRuna\Http\Controllers\PerfilController;
use PusaqRuna\Http\Controllers\CursoController;


/* PUSAQRUNA */

Route::get('/', function () {
    return view('pusaqruna.index');
});

Route::get('/contacto', function () {
    return view('pusaqruna.contacto');
});

/* TINKUY.TEC */

Route::get('/tinkuytec', function () {
    return view('tinkuytec.index');
});

Route::get('/tinkuytec/cursos', function () {
    if(session('usuario'))
        return view('tinkuytec.cursos');
    else
        return redirect('/tinkuytec/registro')->with('info', 'Regístrate gratis para poder acceder a nuestros cursos.');
});

Route::get('/tinkuytec/conocenos', function () {
    return view('tinkuytec.conocetinkuytec');
});

Route::get('/tinkuytec/normas', function () {
    return view('tinkuytec.normastrabajo');
});

Route::get('/tinkuytec/unete', function () {
    return view('tinkuytec.unirsetinkuytec');
});

/* Login y Registro */

Route::get('/tinkuytec/acceso', function () {
    if(!session('usuario'))
        return view('tinkuytec.acceso');
    else
        return redirect('/tinkuytec')->with('info', 'Ya has iniciado sesión.');
});

Route::get('/tinkuytec/registro', function () {
    if(!session('usuario'))
        return view('tinkuytec.registro');
    else
        return redirect('/tinkuytec')->with('info', 'Finalice su sesión en la plataforma para poder registrar a un nuevo usuario.');
});

Route::get('/tinkuytec/salir', function(){
    session(['usuario' => null]);
    session(['urlAvatar' => null]);
    return redirect('/tinkuytec')->with('info', 'Has salido de la plataforma. Vuelve a iniciar sesión para inscribirte a más cursos.');
});

/* Mis cursos */
Route::get('/tinkuytec/miscursos', function(){
    if(session('usuario'))
        return view('tinkuytec.miscursos');
    else
        return redirect('/tinkuytec/acceso')->with('info', 'Inicia sesión para poder ver tus cursos.');
});

Route::get('/tinkuytec/perfil', 'PerfilController@fillPerfil');

Route::post('/tinkuytec/registro', 'Autorizador@registrar');

Route::post('/tinkuytec/acceso', 'Autorizador@acceder');

Route::get('/test', 'Autorizador@test');
//Route::get('/tinkuytec/{curso}', 'CursoController@verDetalleCurso');