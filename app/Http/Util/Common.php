<?php

namespace PusaqRuna\Http\Util;

use Config;
use PusaqRuna\Http\Util\curl;

class Common
{
    public static function get_gravatar($email, $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array())
    {
	    $url  = 'http://www.gravatar.com/avatar/';
	    $url .= md5( strtolower( trim( $email ) ) );
	    $url .= "?s=$s&d=$d&r=$r";
	    if($img) {
	        $url = '<img src="' . $url . '"';
	        foreach($atts as $key => $val)
	            $url .= ' ' . $key . '="' . $val . '"';
	        $url .= ' />';
	    }
	    return $url;
	}

	public static function verificar_recaptcha($user_response = '')
	{
		$curl	= new curl;
		$params = [
			'secret'	=> Config::get('g_recaptcha.secret_key'),
			'response'	=> $user_response
		];
		return json_decode($curl->post('https://www.google.com/recaptcha/api/siteverify', $params))->success;
	}
}
