<?php

namespace PusaqRuna\Http\Controllers;

use Validator;
use PusaqRuna\Http\Controllers\Controller;
use PusaqRuna\Http\Util\Common;
use Illuminate\Http\Request;
use Config;
use Illuminate\Support\Str;
use PusaqRuna\Http\Util\curl;

class Autorizador extends Controller
{
    protected function registrar(Request $request)
    {
        $serverurl = Config::get('moodle.url') . '/webservice/rest/server.php' .
                    '?wstoken=' . Config::get('moodle.token') .
                    '&wsfunction=' . Config::get('moodle.fun.registrar_usuario') .
                    '&moodlewsrestformat=json';

        $dni        = $request->input('dni');
        $nombres    = $request->input('nombres');
        $apellidos  = $request->input('apellidos');
        $correo     = $request->input('correo');
        $pass       = $request->input('password');
        $captcha    = $request->input('g-recaptcha-response');

        $usuario    = [
            'dni'       => $dni,
            'nombres'   => $nombres,
            'apellidos' => $apellidos,
            'correo'    => $correo,
            'password'  => $pass
        ];

        if(Common::verificar_recaptcha($captcha)) {
            $params     = ['usuario' => $usuario];
            $curl       = new curl;
            $response   = json_decode($curl->post($serverurl , $params));
            if(!$response->error_flag)
                return redirect('tinkuytec/acceso')->with('success', 'Usuario registrado con éxito, ya puede ingresar a la plataforma.');
            else
                return redirect('tinkuytec/registro')->with('errores', $response->error_message)->with('recover', [$dni, $nombres, $apellidos, $correo]);
        }
        else
            return redirect('tinkuytec/registro')->with('errores', ['Completa el captcha para poder registrarte'])->with('recover', [$dni, $nombres, $apellidos, $correo]);
    }
    
    protected function acceder(Request $request)
    {
        /*
        $correo = $request->input('correo');
        $pass = $request->input('password');

        $datosUsuario = [
            'correo'    => $correo,
            'pass'      => $pass
        ];

        $reglas = [
            'correo'    => ['required', 'regex:/^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/'],
            'pass'      => 'required|min:6|max:16'
        ];

        $validator = Validator::make($datosUsuario, $reglas);
        $errores = $validator->errors()->all();

        if(count($errores) == 0)
        {
            $u = Usuario::verUsuario($correo)->get();
            
            if(count($u) == 1)
            {
                if(Hash::check($pass, $u[0]['password']))
                {
                    session(['usuario' => $u[0]]);
                    session(['urlAvatar' => Common::get_gravatar(session('usuario')["correo"], 250)]);
                    return redirect('tinkuytec/cursos')->with('success', 'Has accedido a nuestra plataforma correctamente. Echa un vistazo a nuestros cursos disponibles.');
                }
                else
                    return redirect('tinkuytec/acceso')->with('errores', ['Su contraseña es incorrecta.'])->with('recover', $correo);
            }
            else
                return redirect('tinkuytec/acceso')->with('errores', ['El correo electrónico no se encuentra registrado en nuestra plataforma.'])->with('recover', $correo);
        }
        else
            return redirect('tinkuytec/acceso')->with('errores', $errores)->with('recover', $correo);
        */
    }
    
    protected function test()
    {
        
        $user1 = array(
            'username' => 'testusername1',
            'password' => 'testpassword1',
            'createpassword' => 0,
            'firstname' => 'testfirstname1',
            'lastname' => 'testlastname1',
            'email' => 'testemail1@moodle.com',
            'description' => 'Hello World!',
            'city' => 'testcity1',
            'country' => 'au'
        );

        $users = array($user1);
        $params = array('users' => $users);
        $serverurl = $domainname . '/webservice/rest/server.php' .
                    '?wstoken=' . $token .
                    '&wsfunction=' . $functionname .
                    '&moodlewsrestformat=json';
        $curl = new curl;
        $resp = $curl->post($serverurl , $params);

        return view('test')->with('test', json_decode($resp)->errorcode);
    }
}