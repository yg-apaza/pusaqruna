<?php

namespace PusaqRuna\Http\Controllers;

//use PusaqRuna\Usuario;
//use Hash;
use PusaqRuna\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Illuminate\Database\QueryException;

class PerfilController extends Controller
{
	protected function fillPerfil(Request $request)
    {
    	if(session('usuario'))
    	{
	        return view('tinkuytec.perfil');
    	}
	    else
	        return redirect('/tinkuytec/acceso')->with('info', 'Inicia sesión para poder ver tu perfil.');
    }
}